# Global ARGs shared by all stages
ARG DEBIAN_FRONTEND=noninteractive
ARG GOPATH=/usr/local/go

# Build aptly
FROM debian:bookworm-slim as builder
ENV LC_ALL=C.UTF-8
ARG DEBIAN_FRONTEND
ARG GOPATH

RUN apt-get update && \
      apt-get install -y --no-install-recommends \
          build-essential \
          ca-certificates \
          gcc \
          git \
          golang-go \
          libc6-dev

RUN git clone https://gitlab.collabora.com/obs/aptly /tmp/aptly -b collabora/staging \
 && cd /tmp/aptly \
 && make install

FROM debian:bookworm-slim as server
ENV LC_ALL=C.UTF-8
ARG DEBIAN_FRONTEND
ARG GOPATH

COPY --from=builder $GOPATH/bin/aptly /usr/local/bin/aptly
COPY aptly.conf /etc/aptly.conf

RUN apt-get update && \
      apt-get install -y --no-install-recommends \
                bzip2 \
                ca-certificates \
                gnupg \
                gpgv \
                xz-utils

COPY start-aptly.sh /usr/local/bin/

EXPOSE 8080
ENTRYPOINT ["/usr/local/bin/start-aptly.sh"]
