#!/bin/bash

set -e

shopt -s nullglob
for key in /aptly/gpg/*.asc; do
  echo "NOTE: Importing gpg key: $key"
  gpg --import $key
done

exec aptly api serve
